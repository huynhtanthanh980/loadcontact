import 'package:contacts_service/contacts_service.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactService {
  Contact _contact;

  ContactService(this._contact);

  String getPhoneNumber() {
    return (_contact.phones?.length != 0)
        ? _contact.phones?.elementAt(0)?.value.toString()
        : null;
  }

  bool onCallRequest() {
    var number = getPhoneNumber();
    if (number == null) return false;
    launch("tel:$number");
    return true;
  }

  bool onSendMessageRequest() {
    var number = getPhoneNumber();
    if (number == null) return false;
    launch("sms:$number");
    return true;
  }
}
