import 'dart:convert';

import 'package:contacts_service/contacts_service.dart';
import 'package:http/http.dart' as http;

Future<Response> loadList() async {
  final response = await http.get('https://5d12e1a784e9060014576fec.mockapi.io/list');

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    return Response.fromJson(json.decode(response.body));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

Contact contactFromJson(Map<String, dynamic> json) {

  var emailArray = json['emails'];
  var emailArrayLength = emailArray.length;
  Iterable emailIterable = Iterable.generate(emailArrayLength);

  var phoneArray = json['phones'];
  var phoneArrayLength = phoneArray.length;
  Iterable phoneIterable = Iterable.generate(phoneArrayLength);

  var postalAddressesArray = json['postalAddresses'];
  var postalAddressesArrayLength = postalAddressesArray.length;
  Iterable postalAddressesIterable = Iterable.generate(postalAddressesArrayLength);

  return Contact(
      givenName: json['givenName'],
      middleName: json['middleName'],
      prefix: json['prefix'],
      suffix: json['suffix'],
      familyName: json['familyName'],
      company: json['company'],
      jobTitle: json['jobTitle'],
      emails:  (emailArrayLength > 0) ? emailIterable.map((i) => emailArray[i]) : null,
      phones: (phoneArrayLength > 0) ? phoneIterable.map((i) => phoneArray[i]) : null,
      postalAddresses: (postalAddressesArrayLength > 0) ? postalAddressesIterable.map((i) => phoneArray[i]) : null,
  );
}

class Response {
  Iterable<Contact> responseList = new List();

  Response(this.responseList);

  factory Response.fromJson(List<dynamic> json) {
    List<Contact> contact = new List<Contact>();

    contact = json.map((i) => Contact.fromMap(i)).toList();

    return new Response(
      contact,
    );
  }
}