import 'package:flutter/material.dart';
import 'package:loadcontact/Screen/MainScreen.dart';

showNoActionAleartDialog(BuildContext context, String message) {
  showDialog(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(
          "Alert",
          style: TextStyle(
            fontSize: 0.03 * screenHeight,
          ),
        ),
        content: Container(
          height: 0.025 * screenHeight,
          width: 0.4 * screenWidth,
          child: Text(
            message,
            style: TextStyle(
              fontSize: 0.02 * screenHeight,
            ),
          ),
        ),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              "OK",
              style: TextStyle(
                fontSize: 0.02 * screenHeight,
              ),
            ),
          ),
        ],
      );
    },
  );
}

Future<bool> showDeleteConfirmDialog(BuildContext context) async {
  return await showDialog <bool> (
    context: context,
    barrierDismissible: true,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(
          "Alert",
          style: TextStyle(
            fontSize: 0.03 * screenHeight,
          ),
        ),
        content: Container(
          height: 0.025 * screenHeight,
          width: 0.4 * screenWidth,
          child: Text(
            "This contact will be deleted",
            style: TextStyle(
              fontSize: 0.02 * screenHeight,
            ),
          ),
        ),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.of(context).pop(false);
            },
            child: Text(
              "Cancel",
              style: TextStyle(
                fontSize: 0.02 * screenHeight,
              ),
            ),
          ),
          FlatButton(
            onPressed: () async {
              Navigator.of(context).pop(true);
            },
            child: Text(
              "Delete",
              style: TextStyle(
                fontSize: 0.02 * screenHeight,
              ),
            ),
          ),
        ],
      );
    },
  );
}
