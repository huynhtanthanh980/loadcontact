import 'dart:math';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:loadcontact/Model/ScreenController.dart';
import 'package:loadcontact/Screen/CreateScreen.dart';
import 'package:loadcontact/Screen/DetailScreen.dart';
import 'package:loadcontact/Screen/HomeScreen.dart';
import 'package:loadcontact/Screen/SearchScreen.dart';
import 'package:loadcontact/Screen/UpdateScreen.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:vector_math/vector_math.dart' show radians;

var screenHeight, screenWidth;

class MainScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> with SingleTickerProviderStateMixin {
  ScreenController controller = ScreenController();

  @override
  initState() {
    super.initState();
    controller.animationController =
        AnimationController(duration: Duration(milliseconds: 600), vsync: this);
  }

  @override
  void dispose() {
    controller.animationController.dispose();
    super.dispose();
  }

  // ignore: missing_return
  Future<double> whenNotZero(Stream<double> source) async {
    await for (double value in source) {
      if (value > 0) {
        return 1; //if screen size is found, return 1
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: whenNotZero(
          Stream<double>.periodic(Duration(milliseconds: 50),
              (x) => MediaQuery.of(context).size.width),
          //check screen size every 50ms
        ),
        builder: (BuildContext context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data > 0) {
              screenHeight = MediaQuery.of(context).size.height;
              screenWidth = MediaQuery.of(context).size.width;
              controller.leftDx = 0.03 * screenWidth;
              controller.rightDx = 0.83 * screenWidth;
              controller.currentPosition = (controller.currentPosition == null)
                  ? Offset(0.83 * screenWidth, 0.75 * screenHeight)
                  : controller.currentPosition;
              controller.translation = Tween<double>(
                begin: 0.0,
                end: 0.2 * screenWidth,
              ).animate(
                CurvedAnimation(
                    parent: controller.animationController,
                    curve: Curves.elasticOut),
              );
              controller.scale = Tween<double>(
                begin: 1.5,
                end: 0.0,
              ).animate(
                CurvedAnimation(
                    parent: controller.animationController,
                    curve: Curves.fastOutSlowIn),
              );
              controller.rotation = Tween<double>(
                begin: 0, //(controller.isOnRightSide) ? 270.0 : 90.0,
                end: 0, //(controller.isOnRightSide) ? 360.0 : 0.0,
              ).animate(
                CurvedAnimation(
                  parent: controller.animationController,
                  curve: Interval(
                    0.0,
                    0.5,
                    curve: Curves.decelerate,
                  ),
                ),
              );
              return Scaffold(
                resizeToAvoidBottomPadding: false,
                body: ScopedModel<ScreenController>(
                  model: controller,
                  child: SafeArea(
                    child: Container(
                      height: double.infinity,
                      width: double.infinity,
                      child: ScopedModelDescendant<ScreenController>(
                        builder: (context, child, model) =>
                            WillPopScope(
                              child: Stack(
                                children: <Widget>[

                                  getScreen(),

                                  Container(
                                    child: AnimatedBuilder(
                                        animation: controller
                                            .animationController,
                                        builder: (context, animatedWidget) {
                                          return Positioned(
                                            //(0.6 / 2) - (1 * 1.5 /2) = 0.225
                                            top: controller.currentPosition.dy -
                                                ((controller.isOpened)
                                                    ? 0.225 * screenWidth
                                                    : 0),
                                            left: controller.currentPosition
                                                .dx -
                                                ((controller.isOpened)
                                                    ? 0.225 * screenWidth
                                                    : 0),
                                            child: Draggable(
                                              child: Transform.rotate(
                                                angle:
                                                radians(
                                                    controller.rotation.value),
                                                child: Container(
                                                  height: (controller.isOpened)
                                                      ? 0.6 * screenWidth
                                                      : null,
                                                  width: (controller.isOpened)
                                                      ? 0.6 * screenWidth
                                                      : null,
                                                  decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                  ),
                                                  child: Stack(
                                                      alignment: Alignment
                                                          .center,
                                                      children: <Widget>[
                                                        ...getButtonList(),
                                                        Transform.scale(
                                                          //scale.value means its size will scale base con scale var, -1 means reversed animation
                                                          scale:
                                                          controller.scale
                                                              .value -
                                                              1,
                                                          child: Container(
                                                            height: 0.15 *
                                                                screenWidth,
                                                            width: 0.15 *
                                                                screenWidth,
                                                            child: RawMaterialButton(
                                                              child: Icon(
                                                                  controller
                                                                      .floatingButtonCloseIcon),
                                                              onPressed: _close,
                                                              fillColor: controller
                                                                  .floatingButtonCloseColor,
                                                              shape: new CircleBorder(),
                                                        ),
                                                      ),
                                                        ),
                                                        Transform.scale(
                                                          //scale
                                                          scale: controller
                                                              .scale.value,
                                                          child: Container(
                                                            height: 0.1 *
                                                                screenWidth,
                                                            width: 0.1 *
                                                                screenWidth,
                                                            child: RawMaterialButton(
                                                              child: Icon(
                                                                  controller
                                                                      .floatingButtonIcon),
                                                              shape: new CircleBorder(),
                                                              onPressed: _open,
                                                              fillColor: controller
                                                                  .floatingButtonColor,
                                                            ),
                                                          ),
                                                        )
                                                      ]),
                                                ),
                                              ),
                                              onDraggableCanceled: (velocity,
                                                  offset) {
                                                setState(() {
                                                  double dy = 0;

                                                  if (controller.isOpened &&
                                                      offset.dy >
                                                          0.75 * screenHeight) {
                                                    dy = 0.75 * screenHeight;
                                                  } else
                                                  if (controller.isOpened &&
                                                      offset.dy <
                                                          0.1 * screenHeight) {
                                                    dy = 0.1 * screenHeight;
                                                  } else if (offset.dy < 0.0) {
                                                    dy = 0;
                                                  } else if (offset.dy >
                                                      0.9 * screenHeight) {
                                                    dy = 0.9 * screenHeight;
                                                  } else {
                                                    dy = offset.dy;
                                                  }

                                                  controller.isOnRightSide =
                                                  (offset
                                                      .dx >=
                                                      (screenWidth / 2 -
                                                          ((controller.isOpened)
                                                              ? 0.3 *
                                                              screenWidth
                                                              : 0.075 *
                                                              screenWidth)))
                                                      ? true
                                                      : false;

                                                  controller.currentPosition =
                                                      Offset(
                                                          (controller
                                                              .isOnRightSide)
                                                              ? controller
                                                              .rightDx
                                                              : controller
                                                              .leftDx,
                                                          dy);
                                                });
                                              },
                                              //the original child when drag will disappear
                                              childWhenDragging: Container(),
                                              //the child that follows touch gesture
                                              feedback: Container(
                                                height: (controller.isOpened)
                                                    ? 0.6 * screenWidth
                                                    : null,
                                                width: (controller.isOpened)
                                                    ? 0.6 * screenWidth
                                                    : null,
                                                child: Center(
                                                  child: Container(
                                                    height: 0.15 * screenWidth,
                                                    width: 0.15 * screenWidth,
                                                    decoration: BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      color: controller
                                                          .floatingButtonColor,
                                                    ),
                                                    child: Icon(
                                                      controller
                                                          .floatingButtonIcon,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          );
                                        }),
                                  ),
                                ],
                              ),
                              onWillPop: () => onPop(),
                            ),
                      ),
                    ),
                  ),
                ),
              );
            }
          } else {
            return Container();
          }

          return Container();
        });
  }

  getScreen() {
    switch (controller.pageIndex) {
      case 1:
        return Home(controller);
      case 2:
        return DetailScreen(controller);
      case 3:
        return UpdateScreen(controller);
      case 4:
        return CreateScreen(controller);
    }
  }

  onPop() {
    controller.changePage((controller.pageIndex == 3) ? 2 : 1);
  }

  _open() {
    controller.animationController.forward();
    controller.isOpened = true;
    if (controller.currentPosition.dy > 0.75 * screenHeight) {
      controller.currentPosition =
          Offset(controller.currentPosition.dx, 0.75 * screenHeight);
    } else if (controller.currentPosition.dy < 0.1 * screenHeight) {
      controller.currentPosition =
          Offset(controller.currentPosition.dx, 0.1 * screenHeight);
    }
  }

  _close() {
    controller.animationController.reverse();
    controller.isOpened = false;
  }

  _buildButton(int index, double angle, {Color color, IconData icon}) {
    final double rad = radians(angle);
    return Transform(
      transform: Matrix4.identity()
        ..translate((controller.translation.value) * cos(rad),
            (controller.translation.value) * sin(rad)),
      child: Container(
        height: 0.12 * screenWidth,
        width: 0.12 * screenWidth,
        child: RawMaterialButton(
            child: Icon(
              icon,
              color: Colors.white,
            ),
            shape: new CircleBorder(),
            fillColor: color,
            onPressed: () {
              onButtonTap(index);
            }),
      ),
    );
  }

  getButtonList() {
    switch (controller.pageIndex) {
      case 1:
        return [
          _buildButton(1, (controller.isOnRightSide) ? 270 : 270,
              color: Colors.pink, icon: Icons.refresh),
          _buildButton(2, (controller.isOnRightSide) ? 225 : 315,
              color: Colors.red, icon: Icons.person_add),
          _buildButton(3, (controller.isOnRightSide) ? 180 : 360,
              color: Colors.black, icon: Icons.search),
          _buildButton(4, (controller.isOnRightSide) ? 135 : 45,
              color: Colors.indigo,
              icon: (controller.contactList.isAlphabetSort)
                  ? Icons.sort
                  : Icons.sort_by_alpha),
          _buildButton(5, (controller.isOnRightSide) ? 90 : 90,
              color: Colors.pink, icon: FontAwesomeIcons.bong),
        ];
        break;
      case 2:
        return [
          _buildButton(1, (controller.isOnRightSide) ? 225 : 315,
              color: Colors.red, icon: Icons.delete),
          _buildButton(2, (controller.isOnRightSide) ? 180 : 360,
              color: Colors.black, icon: Icons.edit),
          _buildButton(
            3,
            (controller.isOnRightSide) ? 135 : 45,
            color: Colors.indigo,
            icon: Icons.arrow_back,
          ),
        ];
        break;
      case 3:
        return [
          _buildButton(1, (controller.isOnRightSide) ? 225 : 315,
              color: Colors.red, icon: Icons.refresh),
          _buildButton(2, (controller.isOnRightSide) ? 180 : 360,
              color: Colors.black, icon: Icons.save),
          _buildButton(
            3,
            (controller.isOnRightSide) ? 135 : 45,
            color: Colors.indigo,
            icon: Icons.arrow_back,
          ),
        ];
        break;
      case 4:
        return [
          _buildButton(1, (controller.isOnRightSide) ? 225 : 315,
              color: Colors.red, icon: Icons.refresh),
          _buildButton(2, (controller.isOnRightSide) ? 180 : 360,
              color: Colors.black, icon: Icons.add),
          _buildButton(
            3,
            (controller.isOnRightSide) ? 135 : 45,
            color: Colors.indigo,
            icon: Icons.arrow_back,
          ),
        ];
        break;
    }
  }

  onButtonTap(int index) async {
    switch (controller.pageIndex) {
      case 1:
        switch (index) {
          case 1:
            _close();
            controller.refreshContacts();
            break;
          case 2:
            _close();
            controller.changePage(4);
            break;
          case 3:
            _close();
            showSearch(
                context: context,
                delegate: SearchScreen(controller));
            break;
          case 4:
            _close();
            controller.sort();
            break;
          case 5:
            _close();
            break;
        }
        break;
      case 2:
        switch (index) {
          case 1:
            _close();
            await controller.removeContact(context, controller.selectedCustomContact);
            break;
          case 2:
            _close();
            controller.changePage(3);
            break;
          case 3:
            _close();
            controller.changePage(1);
            break;
        }
        break;
      case 3:
        switch (index) {
          case 1:
            _close();
            setState(() {
              controller.resetUpdate();
            });
            break;
          case 2:
            _close();
            await controller.updateContact(context);
            break;
          case 3:
            _close();
            controller.changePage(2);
            break;
        }
        break;
      case 4:
        switch (index) {
          case 1:
            _close();
            setState(() {
              controller.resetCreate();
            });
            break;
          case 2:
            _close();
            controller.addNewContact(context);
            break;
          case 3:
            _close();
            controller.changePage(1);
            break;
        }
        break;
    }
  }
}
