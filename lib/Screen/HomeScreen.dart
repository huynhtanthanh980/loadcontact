import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:loadcontact/Model/ContactList.dart';
import 'package:loadcontact/Model/ScreenController.dart';
import 'package:loadcontact/Screen/MainScreen.dart';
import 'package:loadcontact/Utilities/ContactService.dart';
import 'package:loadcontact/Utilities/CustomAleart.dart';
import 'package:scoped_model/scoped_model.dart';

// ignore: must_be_immutable
class Home extends StatefulWidget {
  ScreenController screenController;

  Home(this.screenController);

  @override
  State<StatefulWidget> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  SlidableController slideController = SlidableController();
  ScrollController _scrollController = ScrollController();

  var subscription;

  _HomeState();

  @override
  initState() {
    super.initState();

    widget.screenController.count = 16;

    //Internet connection listener
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() {
        widget.screenController.refreshContacts();
      });
    });

    _scrollController.addListener(() {
      if (_scrollController.position.atEdge) {
        if (_scrollController.position.pixels != 0) loadMore();
      }
    });
  }

  loadMore() {
    setState(() {
      widget.screenController.count += 15;
      if (widget.screenController.count >
          widget.screenController.contactList.getContactListLength) {
        widget.screenController.count =
            widget.screenController.contactList.getContactListLength;
      }
    });
  }

  @override
  void dispose() {
    subscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: ScopedModel<ScreenController>(
        model: widget.screenController,
        child: ScopedModelDescendant<ScreenController>(
          builder: (context, child, model) => CustomScrollView(
            controller: _scrollController,
            scrollDirection: Axis.vertical,
            physics: AlwaysScrollableScrollPhysics(),
            slivers: <Widget>[
              SliverAppBar(
                expandedHeight: screenHeight / 2.5,
                flexibleSpace: const FlexibleSpaceBar(
                  background: Center(
                    child: Text(
                      "Contact",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 40,
                      ),
                    ),
                  ),
                ),
              ),
              (widget.screenController.contactList.getContactListLength > 0)
                  ? SliverList(
                      delegate: SliverChildBuilderDelegate(
                        (context, index) {
                          CustomContact customContact = widget
                              .screenController.contactList
                              .getContact(index);
                          ContactService contactService =
                              new ContactService(customContact.contact);

                          var dismissThresholds =
                              Map<SlideActionType, double>();
                          dismissThresholds[SlideActionType.primary] = 0.4;
                          dismissThresholds[SlideActionType.secondary] = 0.4;
                          final GlobalKey<_HomeState> expansionTile =
                              new GlobalKey();

                          return Slidable(
                            controller: slideController,
                            key: Key(customContact.contact.displayName),
                            actionPane: SlidableScrollActionPane(),
                            actionExtentRatio: 0.2,
                            dismissal: SlidableDismissal(
                              dismissThresholds: dismissThresholds,
                              child: SlidableDrawerDismissal(),
                              closeOnCanceled: true,
                              onWillDismiss: (actionType) {
                                bool success =
                                    (actionType == SlideActionType.primary)
                                        ? contactService.onCallRequest()
                                        : contactService.onSendMessageRequest();
                                if (!success)
                                  showNoActionAleartDialog(
                                      context, "No Number");
                                if(slideController.activeState != null) {
                                  slideController.activeState.close();
                                }
                                return false;
                              },
                            ),
                            actions: <Widget>[
                              IconSlideAction(
                                caption: 'Call',
                                color: Colors.indigo,
                                icon: Icons.call,
                                onTap: () => contactService.onCallRequest(),
                              ),
                            ],
                            secondaryActions: <Widget>[
                              IconSlideAction(
                                caption: 'Message',
                                color: Colors.red,
                                icon: Icons.message,
                                onTap: () =>
                                    contactService.onSendMessageRequest(),
                              ),
                            ],
                            child: ExpansionTile(
                              key: expansionTile,
                              leading: (customContact.contact.avatar != null &&
                                      customContact.contact.avatar.length > 0)
                                  ? CircleAvatar(
                                      backgroundImage: MemoryImage(
                                          customContact.contact.avatar))
                                  : CircleAvatar(
                                      child: Text(
                                          customContact.contact.initials())),
                              title: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Text(customContact.contact.displayName ?? ""),
                                  Text(
                                    (customContact.isLocal)
                                        ? "  |  Local"
                                        : "  |  Online",
                                    style: TextStyle(
                                      color: Colors.grey,
                                    ),
                                  ),
                                ],
                              ),
                              trailing: Container(
                                height: 0.07 * screenHeight,
                                width: 0.1 * screenWidth,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                ),
                                child: MaterialButton(
                                    child: Icon(Icons.info_outline),
                                    shape: CircleBorder(),
                                    onPressed: () {
                                      widget.screenController
                                              .selectedCustomContact = widget
                                          .screenController.contactList
                                          .getContact(index);
                                      widget.screenController.changePage(2);
                                    }),
                              ),
                              onExpansionChanged: (isOpen) async {
                                if (slideController.activeState != null) {
                                  slideController.activeState.close();
                                }
                              },
                              children: <Widget>[
                                Container(
                                  height: 0.1 * screenHeight,
                                  width: double.infinity,
                                  color: Colors.black12,
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 0.1 * screenWidth),
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        (customContact.contact.phones?.length !=
                                                0)
                                            ? "Phone Number: " +
                                                customContact.contact.phones
                                                    ?.elementAt(0)
                                                    ?.value
                                            : "No Number",
                                      ),
                                      Row(),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                        childCount: (widget.screenController.contactList
                                    .getContactListLength <
                                widget.screenController.count)
                            ? widget.screenController.contactList
                                .getContactListLength
                            : widget.screenController.count,
                      ),
                    )
                  : SliverFillRemaining(
                      fillOverscroll: false,
                      child: Container(
                        height: (0.1) * screenHeight,
                        width: double.infinity,
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
