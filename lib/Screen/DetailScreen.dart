import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:loadcontact/Model/ScreenController.dart';
import 'package:loadcontact/Screen/MainScreen.dart';

class DetailScreen extends StatefulWidget {
  final ScreenController screenController;

  DetailScreen(this.screenController);

  @override
  State<StatefulWidget> createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen>
    with SingleTickerProviderStateMixin {
  double titleTextSize = 20;
  double subTextSize = 20;

  Color titleTextColor = Colors.grey;
  Color subTextColor = Colors.black;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: double.infinity,
          child: Stack(
            children: <Widget>[
              CustomScrollView(
                  scrollDirection: Axis.vertical,
                  physics: AlwaysScrollableScrollPhysics(),
                  slivers: <Widget>[
                    SliverAppBar(
                      expandedHeight: 0.4 * screenHeight,
                      flexibleSpace: const FlexibleSpaceBar(
                        background: Center(
                          child: Text(
                            "Detail",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 40,
                            ),
                          ),
                        ),
                      ),
                      title: Container(
                        width: double.infinity,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            IconButton(
                                icon: Icon(Icons.arrow_back),
                                onPressed: () {
                                  widget.screenController.changePage(1);
                                }),
                          ],
                        ),
                      ),
                    ),
                    SliverList(
                      delegate: SliverChildListDelegate(
                        [
                          Container(
                            width: double.infinity,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                buildListTile(
                                    "Name",
                                    widget
                                        .screenController
                                        .selectedCustomContact
                                        .contact
                                        .givenName,
                                    titleTextColor,
                                    subTextColor,
                                    titleTextSize,
                                    subTextSize),
                                buildListTile(
                                    "Middle name",
                                    widget
                                        .screenController
                                        .selectedCustomContact
                                        .contact
                                        .middleName,
                                    titleTextColor,
                                    subTextColor,
                                    titleTextSize,
                                    subTextSize),
                                buildListTile(
                                    "Family name",
                                    widget
                                        .screenController
                                        .selectedCustomContact
                                        .contact
                                        .familyName,
                                    titleTextColor,
                                    subTextColor,
                                    titleTextSize,
                                    subTextSize),
                                buildListTile(
                                    "Prefix",
                                    widget.screenController
                                        .selectedCustomContact.contact.prefix,
                                    titleTextColor,
                                    subTextColor,
                                    titleTextSize,
                                    subTextSize),
                                buildListTile(
                                    "Suffix",
                                    widget.screenController
                                        .selectedCustomContact.contact.suffix,
                                    titleTextColor,
                                    subTextColor,
                                    titleTextSize,
                                    subTextSize),
                                buildListTile(
                                    "Company",
                                    widget.screenController
                                        .selectedCustomContact.contact.company,
                                    titleTextColor,
                                    subTextColor,
                                    titleTextSize,
                                    subTextSize),
                                buildListTile(
                                    "Job",
                                    widget.screenController
                                        .selectedCustomContact.contact.jobTitle,
                                    titleTextColor,
                                    subTextColor,
                                    titleTextSize,
                                    subTextSize),
                                AddressesTile(
                                    widget
                                        .screenController
                                        .selectedCustomContact
                                        .contact
                                        .postalAddresses,
                                    titleTextColor,
                                    subTextColor,
                                    titleTextSize,
                                    subTextSize),
                                ItemsTile(
                                    "Phones",
                                    widget.screenController
                                        .selectedCustomContact.contact.phones,
                                    titleTextColor,
                                    subTextColor,
                                    titleTextSize,
                                    subTextSize),
                                ItemsTile(
                                    "Emails",
                                    widget.screenController
                                        .selectedCustomContact.contact.emails,
                                    titleTextColor,
                                    subTextColor,
                                    titleTextSize,
                                    subTextSize),
                                SizedBox(
                                  height: screenHeight / 4,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ]),
            ],
          ),
        ),
      ),
    );
  }
}

buildListTile(String title, String content, Color titleTextColor,
    Color subTextColor, double titleTextSize, double subTextSize) {
  return ListTile(
    title: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          title,
          style: TextStyle(
            color: titleTextColor,
            fontSize: titleTextSize,
          ),
        ),
        Container(
          width: screenWidth / 2,
          alignment: Alignment.centerRight,
          child: Text(
            content ?? "",
            style: TextStyle(
              color: subTextColor,
              fontSize: subTextSize,
            ),
            overflow: TextOverflow.visible,
          ),
        ),
      ],
    ),
  );
}

class AddressesTile extends StatelessWidget {
  AddressesTile(this._addresses, this.titleTextColor, this.subTextColor,
      this.titleTextSize, this.subTextSize);

  final double titleTextSize;
  final double subTextSize;

  final Color titleTextColor;
  final Color subTextColor;

  final Iterable<PostalAddress> _addresses;

  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        ListTile(
            title: Text(
              "Address",
              style: TextStyle(
                color: titleTextColor,
                fontSize: titleTextSize,
              ),
            )),
        _addresses == null ? Container() : Flex(
          direction: Axis.vertical,
          children: _addresses
              .map((a) =>
              Padding(
                  padding:EdgeInsets.only(left: 16.0),
                  child: ExpansionTile(
                    title: Text(
                      a.label ?? "",
                      style: TextStyle(
                        color: titleTextColor,
                        fontSize: titleTextSize,
                      ),
                    ),
                    children: <Widget>[
                      buildListTile("Street", a.street, subTextColor,
                          subTextColor, titleTextSize, subTextSize),
                      buildListTile("Postcode", a.postcode, subTextColor,
                          subTextColor, titleTextSize, subTextSize),
                      buildListTile("City", a.city, subTextColor, subTextColor,
                          titleTextSize, subTextSize),
                      buildListTile("Region", a.region, subTextColor,
                          subTextColor, titleTextSize, subTextSize),
                      buildListTile("Country", a.country, subTextColor,
                          subTextColor, titleTextSize, subTextSize),
                    ],
                  )))
              .toList(),
        ),
      ],
    );
  }
}

class ItemsTile extends StatelessWidget {
  ItemsTile(this._title, this._items, this.titleTextColor, this.subTextColor,
      this.titleTextSize, this.subTextSize);

  final Iterable<Item> _items;
  final String _title;

  final double titleTextSize;
  final double subTextSize;

  final Color titleTextColor;
  final Color subTextColor;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        ListTile(
            title: Text(
              _title,
              style: TextStyle(
                color: titleTextColor,
                fontSize: titleTextSize,
              ),
            )),
        _items == null ? Container() : Column(
          children: _items
              .map(
                (i) =>
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: ListTile(
                    title: Text(
                      i.label ?? "",
                      style: TextStyle(
                        color: titleTextColor,
                        fontSize: titleTextSize,
                      ),
                    ),
                    trailing: Container(
                      width: screenWidth / 2,
                      alignment: Alignment(1, 0),
                      child: Text(
                        i.value ?? "",
                        style: TextStyle(
                          color: subTextColor,
                          fontSize: subTextSize,
                        ),
                        overflow: TextOverflow.visible,
                      ),
                    ),
                  ),
                ),
          )
              .toList(),
        ),
      ],
    );
  }
}
