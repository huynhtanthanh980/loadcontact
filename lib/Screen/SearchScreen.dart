import 'package:flutter/material.dart';
import 'package:loadcontact/Model/ContactList.dart';
import 'package:loadcontact/Model/ScreenController.dart';
import 'package:loadcontact/Screen/MainScreen.dart';
import 'package:loadcontact/Utilities/ContactService.dart';

class SearchScreen extends SearchDelegate<CustomContact> {
  ScreenController screenController;

  SearchScreen(this.screenController);

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = "";
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () => close(context, null),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    final result = screenController.contactList.searchContact(
        query.toLowerCase()).toList();

    return (result.isEmpty)
        ? Container(
            height: double.infinity,
            width: double.infinity,
            child: Center(
              child: Text(
                "No Result",
                style: TextStyle(
                    fontSize: 0.03 * screenHeight, color: Colors.grey),
              ),
            ),
          )
        : ListView.builder(
            itemCount: result.length,
            itemBuilder: (context, index) {
              var customContact = result[index];
              var contactService = new ContactService(customContact.contact);

              return Container(
                height: 0.1 * screenHeight,
                width: double.infinity,
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 0.76 * screenWidth,
                      child: ListTile(
                          leading: (customContact.contact.avatar != null &&
                                  customContact.contact.avatar.length > 0)
                              ? CircleAvatar(
                                  backgroundImage:
                                      MemoryImage(customContact.contact.avatar))
                              : CircleAvatar(
                                  child:
                                      Text(customContact.contact.initials())),
                          title: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Text(customContact.contact.displayName ?? ""),
                              Text(
                                (customContact.isLocal)
                                    ? "  |  Local"
                                    : "  |  Online",
                                style: TextStyle(
                                  color: Colors.grey,
                                ),
                              ),
                            ],
                          ),
                          subtitle: Text(
                            contactService.getPhoneNumber() ?? "No Number",
                          ),
                          onTap: () {
                            screenController.changePage(2);
                          }),
                    ),
                    Container(
                        height: 0.05 * screenHeight,
                        width: 0.12 * screenWidth,
                        child: FloatingActionButton(
                          heroTag: UniqueKey(),
                          child: Icon(Icons.call),
                          onPressed: () => contactService.onCallRequest(),
                          elevation: 0.0,
                        )),
                    Container(
                      height: 0.05 * screenHeight,
                      width: 0.12 * screenWidth,
                      child: FloatingActionButton(
                        heroTag: UniqueKey(),
                        child: Icon(Icons.message),
                        onPressed: () => contactService.onSendMessageRequest(),
                        elevation: 0.0,
                        backgroundColor: Colors.red,
                      ),
                    ),
                  ],
                ),
              );
            },
          );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final result = screenController.contactList.searchContact(
        query.toLowerCase()).toList();

    return (result.isEmpty)
        ? Container(
            height: double.infinity,
            width: double.infinity,
            child: Center(
              child: Text(
                "No Result",
                style: TextStyle(
                    fontSize: 0.03 * screenHeight, color: Colors.grey),
              ),
            ),
          )
        : ListView.builder(
            itemCount: result.length,
            itemBuilder: (context, index) {
              var customContact = result[index];
              var contactService = new ContactService(customContact.contact);

              return Container(
                height: 0.1 * screenHeight,
                width: double.infinity,
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 0.76 * screenWidth,
                      child: ListTile(
                          leading: (customContact.contact.avatar != null &&
                                  customContact.contact.avatar.length > 0)
                              ? CircleAvatar(
                                  backgroundImage:
                                      MemoryImage(customContact.contact.avatar))
                              : CircleAvatar(
                                  child:
                                      Text(customContact.contact.initials())),
                          title: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Text(customContact.contact.displayName ?? ""),
                              Text(
                                (customContact.isLocal)
                                    ? "  |  Local"
                                    : "  |  Online",
                                style: TextStyle(
                                  color: Colors.grey,
                                ),
                              ),
                            ],
                          ),
                          subtitle: Text(
                            contactService.getPhoneNumber() ?? "No Number",
                          ),
                          onTap: () {
                            screenController.changePage(2);
                          }),
                    ),
                    Container(
                        height: 0.05 * screenHeight,
                        width: 0.12 * screenWidth,
                        child: FloatingActionButton(
                          heroTag: UniqueKey(),
                          child: Icon(Icons.call),
                          onPressed: () => contactService.onCallRequest(),
                          elevation: 0.0,
                        )),
                    Container(
                      height: 0.05 * screenHeight,
                      width: 0.12 * screenWidth,
                      child: FloatingActionButton(
                        heroTag: UniqueKey(),
                        child: Icon(Icons.message),
                        onPressed: () => contactService.onSendMessageRequest(),
                        elevation: 0.0,
                        backgroundColor: Colors.red,
                      ),
                    ),
                  ],
                ),
              );
            },
          );
  }
}
