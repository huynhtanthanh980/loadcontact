import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:loadcontact/Model/AddressData.dart';
import 'package:loadcontact/Model/ScreenController.dart';
import 'package:loadcontact/Screen/MainScreen.dart';

class UpdateScreen extends StatefulWidget {
  final ScreenController screenController;

  UpdateScreen(this.screenController);

  @override
  _UpdateScreenState createState() => _UpdateScreenState();
}

class _UpdateScreenState extends State<UpdateScreen>
    with SingleTickerProviderStateMixin {

  double textSize = 20;

  Contact contact;

  AddressList addressList = AddressList();
  PhoneList phoneList = PhoneList();
  EmailList emailList = EmailList();

  @override
  void initState() {
    super.initState();
    contact = widget.screenController.selectedCustomContact.contact;

    widget.screenController.selectedCustomContactAddressList = addressList;
    widget.screenController.selectedCustomContactPhoneList = phoneList;
    widget.screenController.selectedCustomContactEmailList = emailList;

    for (PostalAddress postalAddress in contact.postalAddresses) {
      addressList.fill(AddressData(addressList.itemCount++, postalAddress.label, postalAddress));
    }

    for (Item item in contact.phones) {
      phoneList.fill(PhoneData(phoneList.itemCount++, item.label, item.value));
    }

    for (Item item in contact.emails) {
      emailList.fill(EmailData(emailList.itemCount++, item.label, item.value));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: double.infinity,
          child: Stack(
            children: <Widget>[
              CustomScrollView(
                  scrollDirection: Axis.vertical,
                  physics: AlwaysScrollableScrollPhysics(),
                  slivers: <Widget>[
                    SliverAppBar(
                      expandedHeight: 0.4 * screenHeight,
                      flexibleSpace: const FlexibleSpaceBar(
                        background: Center(
                          child: Text(
                            "Update Contact",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 40,
                            ),
                          ),
                        ),
                      ),
                      title: Container(
                        width: double.infinity,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            IconButton(
                                icon: Icon(Icons.arrow_back),
                                onPressed: () {
                                  widget.screenController.changePage(2);
                                }),
                            IconButton(
                              icon: Icon(Icons.save),
                              onPressed: () => widget.screenController
                                  .updateContact(context),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SliverList(
                      delegate: SliverChildListDelegate(
                        [
                          Container(
                            width: double.infinity,
                            padding: EdgeInsets.symmetric(
                                horizontal: 0.05 * screenWidth),
                            child: Form(
                              key: widget.screenController.formKey,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  TextFormField(
                                    initialValue: contact.givenName ?? "",
                                    decoration: const InputDecoration(
                                      labelText: 'First name',
                                    ),
                                    style: TextStyle(
                                      fontSize: textSize,
                                    ),
                                    onSaved: (v) => contact.givenName = v,
                                  ),
                                  TextFormField(
                                    initialValue: contact.middleName ?? "",
                                    decoration: const InputDecoration(
                                        labelText: 'Middle name'),
                                    style: TextStyle(
                                      fontSize: textSize,
                                    ),
                                    onSaved: (v) => contact.middleName = v,
                                  ),
                                  TextFormField(
                                    initialValue: contact.familyName ?? "",
                                    decoration: const InputDecoration(
                                        labelText: 'Last name'),
                                    style: TextStyle(
                                      fontSize: textSize,
                                    ),
                                    onSaved: (v) => contact.familyName = v,
                                  ),
                                  TextFormField(
                                    initialValue: contact.prefix ?? "",
                                    decoration: const InputDecoration(
                                        labelText: 'Prefix'),
                                    style: TextStyle(
                                      fontSize: textSize,
                                    ),
                                    onSaved: (v) => contact.prefix = v,
                                  ),
                                  TextFormField(
                                    initialValue: contact.suffix ?? "",
                                    decoration: const InputDecoration(
                                        labelText: 'Suffix'),
                                    style: TextStyle(
                                      fontSize: textSize,
                                    ),
                                    onSaved: (v) => contact.suffix = v,
                                  ),
                                  SizedBox(
                                    height: 20.0,
                                  ),

                                  Text("Phone", style: TextStyle(fontSize: 16.0),),

                                  ListView.builder(
                                    shrinkWrap: true,
                                    physics: NeverScrollableScrollPhysics(),
                                    itemCount: phoneList.list.length,
                                    itemBuilder: (context, index) {
                                      return Column(
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              DropdownButton<String>(
                                                value: phoneList
                                                    .list[index].phoneType,
                                                icon: Icon(
                                                    Icons.keyboard_arrow_down),
                                                iconSize: 24,
                                                onChanged: (String newValue) {
                                                  setState(() {
                                                    phoneList.list[index]
                                                        .phoneType = newValue;
                                                  });
                                                },
                                                items: <String>[
                                                  'mobile',
                                                  'home',
                                                  'other'
                                                ].map<DropdownMenuItem<String>>(
                                                        (String value) {
                                                      return DropdownMenuItem<
                                                          String>(
                                                        value: value,
                                                        child: Text(value),
                                                      );
                                                    }).toList(),
                                              ),
                                              FlatButton(
                                                child: Icon(Icons.cancel),
                                                onPressed: () {
                                                  setState(() {
                                                    phoneList.remove(phoneList
                                                        .list[index]
                                                        .identifier);
                                                  });
                                                },
                                              ),
                                            ],
                                          ),
                                          TextFormField(
                                            key: Key("Phone" + phoneList.list[index].identifier.toString()),
                                            initialValue: phoneList.list[index].phoneNumber ?? "",
                                            decoration: const InputDecoration(
                                                labelText: 'Phone'),
                                            onSaved: (v) {
                                              phoneList.list[index]
                                                  .phoneNumber = v ?? "";
                                            },
                                            keyboardType:
                                            TextInputType.phone,
                                          ),
                                        ],
                                      );
                                    },
                                  ),
                                  FlatButton(
                                    child: Icon(Icons.add),
                                    onPressed: () {
                                      setState(() {
                                        phoneList.add();
                                      });
                                    },
                                  ),

                                  SizedBox(
                                    height: 20.0,
                                  ),

                                  Text("Email", style: TextStyle(fontSize: 16.0),),

                                  ListView.builder(
                                    shrinkWrap: true,
                                    physics: NeverScrollableScrollPhysics(),
                                    itemCount: emailList.list.length,
                                    itemBuilder: (context, index) {
                                      return Column(
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              DropdownButton<String>(
                                                value: emailList
                                                    .list[index].emailType,
                                                icon: Icon(
                                                    Icons.keyboard_arrow_down),
                                                iconSize: 24,
                                                onChanged: (String newValue) {
                                                  setState(() {
                                                    emailList.list[index]
                                                        .emailType = newValue;
                                                  });
                                                },
                                                items: <String>[
                                                  'home',
                                                  'work',
                                                  'other'
                                                ].map<DropdownMenuItem<String>>(
                                                        (String value) {
                                                      return DropdownMenuItem<
                                                          String>(
                                                        value: value,
                                                        child: Text(value),
                                                      );
                                                    }).toList(),
                                              ),
                                              FlatButton(
                                                child: Icon(Icons.cancel),
                                                onPressed: () {
                                                  setState(() {
                                                    emailList.remove(emailList
                                                        .list[index]
                                                        .identifier);
                                                  });
                                                },
                                              ),
                                            ],
                                          ),
                                          TextFormField(
                                            key: Key("Email" + emailList.list[index].identifier.toString()),
                                            initialValue: emailList.list[index].email ?? "",
                                            decoration: const InputDecoration(
                                                labelText: 'E-mail'),
                                            onSaved: (v) {
                                              emailList.list[index]
                                                  .email = v ?? "";
                                            },
                                            keyboardType:
                                            TextInputType.emailAddress,
                                          ),
                                        ],
                                      );
                                    },
                                  ),
                                  FlatButton(
                                    child: Icon(Icons.add),
                                    onPressed: () {
                                      setState(() {
                                        emailList.add();
                                      });
                                    },
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),

                                  TextFormField(
                                    initialValue: contact.company ?? "",
                                    decoration: const InputDecoration(
                                        labelText: 'Company'),
                                    onSaved: (v) => widget
                                        .screenController
                                        .selectedCustomContact
                                        .contact
                                        .company = v ?? "",
                                  ),
                                  TextFormField(
                                    initialValue: contact.jobTitle ?? "",
                                    decoration:
                                    const InputDecoration(labelText: 'Job'),
                                    onSaved: (v) => widget
                                        .screenController
                                        .selectedCustomContact
                                        .contact
                                        .jobTitle = v ?? "",
                                  ),
                                  SizedBox(
                                    height: 20.0,
                                  ),

                                  Text("Address", style: TextStyle(fontSize: 16.0),),

                                  ListView.builder(
                                    shrinkWrap: true,
                                    physics: NeverScrollableScrollPhysics(),
                                    itemCount: addressList.list.length,
                                    itemBuilder: (context, index) {
                                      return Column(
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              DropdownButton<String>(
                                                value: addressList
                                                    .list[index].addressType,
                                                icon: Icon(
                                                    Icons.keyboard_arrow_down),
                                                iconSize: 24,
                                                onChanged: (String newValue) {
                                                  setState(() {
                                                    addressList.list[index]
                                                        .addressType = newValue;
                                                  });
                                                },
                                                items: <String>[
                                                  'home',
                                                  'work',
                                                  'other'
                                                ].map<DropdownMenuItem<String>>(
                                                        (String value) {
                                                      return DropdownMenuItem<
                                                          String>(
                                                        value: value,
                                                        child: Text(value),
                                                      );
                                                    }).toList(),
                                              ),
                                              FlatButton(
                                                child: Icon(Icons.cancel),
                                                onPressed: () {
                                                  setState(() {
                                                    addressList.remove(
                                                        addressList.list[index]
                                                            .identifier);
                                                  });
                                                },
                                              ),
                                            ],
                                          ),
                                          TextFormField(
                                            key: Key("Street" + addressList.list[index].identifier.toString()),
                                            initialValue: addressList
                                                .list[index]
                                                .postalAddress
                                                .street ??
                                                "",
                                            decoration: const InputDecoration(
                                                labelText: 'Street'),
                                            onSaved: (v) {
                                              addressList
                                                  .list[index]
                                                  .postalAddress
                                                  .street = v ?? "";
                                            },
                                          ),
                                          TextFormField(
                                            key: Key("City" + addressList.list[index].identifier.toString()),
                                            initialValue: addressList
                                                .list[index]
                                                .postalAddress
                                                .city ??
                                                "",
                                            decoration: const InputDecoration(
                                                labelText: 'City'),
                                            onSaved: (v) => addressList
                                                .list[index]
                                                .postalAddress
                                                .city = v ?? "",
                                          ),
                                          TextFormField(
                                            key: Key("Region" + addressList.list[index].identifier.toString()),
                                            initialValue: addressList
                                                .list[index]
                                                .postalAddress
                                                .region ??
                                                "",
                                            decoration: const InputDecoration(
                                                labelText: 'Region'),
                                            onSaved: (v) => addressList
                                                .list[index]
                                                .postalAddress
                                                .region = v ?? "",
                                          ),
                                          TextFormField(
                                            key: Key("PostalCode" + addressList.list[index].identifier.toString()),
                                            initialValue: addressList
                                                .list[index]
                                                .postalAddress
                                                .postcode ??
                                                "",
                                            decoration: const InputDecoration(
                                                labelText: 'Postal code'),
                                            onSaved: (v) => addressList
                                                .list[index]
                                                .postalAddress
                                                .postcode = v ?? "",
                                          ),
                                          TextFormField(
                                            key: Key("Country" + addressList.list[index].identifier.toString()),
                                            initialValue: addressList
                                                .list[index]
                                                .postalAddress
                                                .country ??
                                                "",
                                            decoration: const InputDecoration(
                                                labelText: 'Country'),
                                            onSaved: (v) => addressList
                                                .list[index]
                                                .postalAddress
                                                .country = v ?? "",
                                          ),
                                        ],
                                      );
                                    },
                                  ),
                                  FlatButton(
                                    child: Icon(Icons.add),
                                    onPressed: () {
                                      setState(() {
                                        addressList.add();
                                      });
                                    },
                                  ),
                                  SizedBox(
                                    height: screenHeight / 4,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ]),
            ],
          ),
        ),
      ),
    );
  }
}
