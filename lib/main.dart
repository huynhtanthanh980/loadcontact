import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:loadcontact/Screen/MainScreen.dart';

void main() => runApp(LoadContact());

class LoadContact extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //Force Portrait mode
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MainScreen(),
      routes: <String, WidgetBuilder>{
        '/Home': (BuildContext context) => new MainScreen(),
      },
    );
  }
}