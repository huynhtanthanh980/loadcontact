import 'package:contacts_service/contacts_service.dart';

class AddressData {
  int identifier;
  String addressType;
  PostalAddress postalAddress;

  AddressData(this.identifier, this.addressType, this.postalAddress);
}

class AddressList {
  int itemCount = 0;
  List<AddressData> list = List<AddressData>();
  
  add() {
    list.add(AddressData(itemCount++, "home", new PostalAddress()));
  }

  fill(AddressData contactData){
    list.add(contactData);
  }
  
  remove(int identifier) {
    list.removeWhere((item) => item.identifier == identifier);
  }

  wipe() {
    itemCount = 0;
    list = List<AddressData>();
  }
}

class PhoneData {
  int identifier;
  String phoneType;
  String phoneNumber;

  PhoneData(this.identifier, this.phoneType, this.phoneNumber);
}

class PhoneList {
  int itemCount = 0;
  List<PhoneData> list = List<PhoneData>();

  add() {
    list.add(PhoneData(itemCount++, "mobile", ""));
  }

  fill(PhoneData phoneData){
    list.add(phoneData);
  }

  remove(int identifier) {
    list.removeWhere((item) => item.identifier == identifier);
    print("removed $identifier");
    print(list.toString());
  }

  wipe() {
    itemCount = 0;
    list = List<PhoneData>();
  }
}

class EmailData {
  int identifier;
  String emailType;
  String email;

  EmailData(this.identifier, this.emailType, this.email);
}

class EmailList {
  int itemCount = 0;
  List<EmailData> list = List<EmailData>();

  add() {
    list.add(EmailData(itemCount++, "home", ""));
  }

  fill(EmailData emailData){
    list.add(emailData);
  }

  remove(int identifier) {
    list.removeWhere((item) => item.identifier == identifier);
  }

  wipe() {
    itemCount = 0;
    list = List<EmailData>();
  }
}