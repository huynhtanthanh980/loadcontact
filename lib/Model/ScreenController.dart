import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:loadcontact/Model/AddressData.dart';
import 'package:loadcontact/Model/ContactList.dart';
import 'package:loadcontact/Utilities/CustomAleart.dart';
import 'package:scoped_model/scoped_model.dart';

class ScreenController extends Model {
  ContactList contactList = ContactList();

  int count;

  int pageIndex = 1;

  AnimationController animationController;

  bool isOnRightSide = true;
  bool isOpened = false;

  Animation<double> scale;
  Animation<double> translation;
  Animation<double> rotation;

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  double leftDx;
  double rightDx;

  Offset currentPosition;

  IconData floatingButtonIcon = Icons.menu;
  IconData floatingButtonCloseIcon = Icons.close;
  Color floatingButtonColor = Colors.redAccent;
  Color floatingButtonCloseColor = Colors.red;

  CustomContact selectedCustomContact;
  AddressList selectedCustomContactAddressList;
  PhoneList selectedCustomContactPhoneList;
  EmailList selectedCustomContactEmailList;

  addNewContact(BuildContext context) {
    formKey.currentState.save();

    if (contactList.checkValidContact(selectedCustomContact)) {
      showNoActionAleartDialog(context, "Duplicated name");
      return;
    } else if (checkValidName()) {
      addAddress();
      addPhone();
      addEmail();

      ContactsService.addContact(selectedCustomContact.contact);

      changePage(1);

      return;
    }

    showNoActionAleartDialog(context, "Invalid name");
  }

  updateContact(BuildContext context) async {
    String originalName = getName(selectedCustomContact.contact);
    formKey.currentState.save();
    String newName = getName(selectedCustomContact.contact);

    if (originalName != newName &&
        contactList.checkValidContact(selectedCustomContact)) {
      showNoActionAleartDialog(context, "Duplicated name");
      return;
    } else if (checkValidName()) {

      addAddress();
      addPhone();
      addEmail();

      await ContactsService.updateContact(selectedCustomContact.contact)
          .then((_) => changePage(2));
      return;
    }

    showNoActionAleartDialog(context, "Invalid name");
  }

  checkValidName() {
    return (selectedCustomContact.contact.givenName.isNotEmpty) |
        (selectedCustomContact.contact.middleName.isNotEmpty) |
        (selectedCustomContact.contact.familyName.isNotEmpty);
  }

  getName(Contact contact) {
    return contact.givenName.toString() +
        contact.middleName.toString() +
        contact.familyName.toString();
  }

  addAddress() {
    var list = List<PostalAddress>();
    Iterable postalAddressesList =
        Iterable.generate(selectedCustomContactAddressList.list.length);
    for (int i = 0; i < selectedCustomContactAddressList.list.length; i++) {
      PostalAddress address = PostalAddress(
          label: selectedCustomContactAddressList.list[i].addressType);
      address.street =
          selectedCustomContactAddressList.list[i].postalAddress.street;
      address.city =
          selectedCustomContactAddressList.list[i].postalAddress.city;
      address.country =
          selectedCustomContactAddressList.list[i].postalAddress.country;
      address.postcode =
          selectedCustomContactAddressList.list[i].postalAddress.postcode;
      address.region =
          selectedCustomContactAddressList.list[i].postalAddress.region;
      list.add(address);
    }
    selectedCustomContact.contact.postalAddresses =
        (selectedCustomContactAddressList.list.length > 0)
            ? postalAddressesList.map((i) => list[i])
            : null;
  }

  addPhone() {
    var list = List<Item>();
    Iterable phoneList =
        Iterable.generate(selectedCustomContactPhoneList.list.length);
    for (int i = 0; i < selectedCustomContactPhoneList.list.length; i++) {
      list.add(Item(
          label: selectedCustomContactPhoneList.list[i].phoneType,
          value: selectedCustomContactPhoneList.list[i].phoneNumber));
    }
    selectedCustomContact.contact.phones =
        (selectedCustomContactPhoneList.list.length > 0)
            ? phoneList.map((i) => list[i])
            : null;
  }

  addEmail() {
    var list = List<Item>();
    Iterable emailList =
        Iterable.generate(selectedCustomContactEmailList.list.length);
    for (int i = 0; i < selectedCustomContactEmailList.list.length; i++) {
      list.add(Item(
          label: selectedCustomContactEmailList.list[i].emailType,
          value: selectedCustomContactEmailList.list[i].email));
    }
    selectedCustomContact.contact.emails =
        (selectedCustomContactEmailList.list.length > 0)
            ? emailList.map((i) => list[i])
            : null;
  }

  removeContact(BuildContext context, CustomContact customContact) async {
    bool result = await showDeleteConfirmDialog(context);
    if (result) {
      if (customContact.isLocal) {
        await ContactsService.deleteContact(customContact.contact);
      } else {
        //Todo: add remove online contact API
      }
      changePage(1);
    }
  }

  resetCreate() {
    formKey.currentState.reset();

    selectedCustomContactAddressList.wipe();
    selectedCustomContactPhoneList.wipe();
    selectedCustomContactEmailList.wipe();
  }

  resetUpdate() {
    formKey.currentState.reset();

    selectedCustomContactAddressList.wipe();
    selectedCustomContactPhoneList.wipe();
    selectedCustomContactEmailList.wipe();

    for (PostalAddress postalAddress in selectedCustomContact.contact.postalAddresses) {
      selectedCustomContactAddressList.fill(AddressData(selectedCustomContactAddressList.itemCount++, postalAddress.label, postalAddress));
    }

    for (Item item in selectedCustomContact.contact.phones) {
      selectedCustomContactPhoneList.fill(PhoneData(selectedCustomContactPhoneList.itemCount++, item.label, item.value));
    }

    for (Item item in selectedCustomContact.contact.emails) {
      selectedCustomContactEmailList.fill(EmailData(selectedCustomContactEmailList.itemCount++, item.label, item.value));
    }
  }

  refreshContacts() async {
    count = 16;
    contactList.contactList = List<CustomContact>();
    contactList.contactNameList = Map<String, CustomContact>();
    await contactList.loadLocalContact();
    notifyListeners();
    await contactList.loadOnlineContact();
    notifyListeners();
  }

  sort() {
    contactList.toggleSort();
    notifyListeners();
  }

  changePage(int index) async {
    pageIndex = index;
    notifyListeners();
  }
}
