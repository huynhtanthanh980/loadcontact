import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/services.dart';
import 'package:loadcontact/Utilities/JsonLoad.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:scoped_model/scoped_model.dart';

class CustomContact {
  Contact contact;
  bool isLocal;

  CustomContact(
    this.contact,
    this.isLocal,
  );
}

class ContactList extends Model {
  List<CustomContact> contactList = List<CustomContact>();
  Map<String, CustomContact> contactNameList = Map<String, CustomContact>();

  int get getContactListLength => contactList.length;

  bool isAlphabetSort = false;

  CustomContact getContact(int index) {
    return contactList[index];
  }

  bool checkValidContact(CustomContact customContact) {

    String name = customContact.contact.givenName.toLowerCase() +
        (customContact.contact.middleName == "" ?  "" : " " + customContact.contact.middleName.toLowerCase()) +
        (customContact.contact.familyName == "" ?  "" : " " + customContact.contact.familyName.toLowerCase());

    return contactNameList[name.trimRight()] !=
        null;
  }

  sort() {
    contactList.sort((customContact1, customContact2) {
      String compare1 =
          ((!isAlphabetSort) ? ((customContact1.isLocal) ? "a" : "b") : "") +
              customContact1.contact.displayName.toLowerCase();
      String compare2 =
          ((!isAlphabetSort) ? ((customContact2.isLocal) ? "a" : "b") : "") +
              customContact2.contact.displayName.toLowerCase();
      int indexCompare = sortIndex(compare1, compare2);
      if (indexCompare != 0) return indexCompare;
      return compare1.compareTo(compare2);
    });
  }

  sortIndex(String str1, String str2) {
    int i = 0;

    //Split the index
    while (str1[i] == str2[i]) {
      i++;
      if (i == str1.length || i == str2.length) {
        return 0;
      }
    }

    //"a1" and "a 1"
    if (str1[i] == " " || str2[i] == " ") return 0;

    String subStr1 = str1.substring(i, str1.length);
    String subStr2 = str2.substring(i, str2.length);

    //if index1 > index2 => compare return x > 0
    if (isNumeric(subStr1) && isNumeric(subStr2)) {
      return (double.parse(subStr1) > double.parse(subStr2)) ? 1 : -1;
    }

    return 0;
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    // ignore: deprecated_member_use
    return double.tryParse(s) != null;
  }

  toggleSort() {
    isAlphabetSort = !isAlphabetSort;
    sort();
  }

  searchContact(String input) {
    return contactList
        .where((a) => a.contact.displayName.toLowerCase().contains(input));
  }

  loadLocalContact() async {
    PermissionStatus permissionStatus = await _getContactPermission();
    if (permissionStatus == PermissionStatus.granted) {
      Iterable<Contact> contacts = await ContactsService.getContacts();
      for (var x in contacts) {
        var customContact = CustomContact(
          x,
          true,
        );
        contactList.add(customContact);

        String name = (x.givenName?.toLowerCase() ?? "") +
            (x.middleName == null ?  "" : " " + x.middleName.toLowerCase()) +
            (x.familyName == null ?  "" : " " + x.familyName.toLowerCase());

        contactNameList[name.trimRight()] = customContact;
      }
      sort();
    } else {
      _handleInvalidPermissions(permissionStatus);
    }
  }

  loadOnlineContact() async {

    Response response = await loadList();

    for (var x in response.responseList) {
      var customContact = CustomContact(
        x,
        false,
      );
      contactList.add(customContact);

      String name = (x.givenName?.toLowerCase() ?? "") +
          (x.middleName == null ?  "" : " " + x.middleName.toLowerCase()) +
          (x.familyName == null ?  "" : " " + x.familyName.toLowerCase());

      contactNameList[name.trimRight()] = customContact;
    }
    sort();
  }

  Future<PermissionStatus> _getContactPermission() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.contacts);
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.disabled) {
      Map<PermissionGroup, PermissionStatus> permissionStatus =
          await PermissionHandler()
              .requestPermissions([PermissionGroup.contacts]);
      return permissionStatus[PermissionGroup.contacts] ??
          PermissionStatus.unknown;
    } else {
      return permission;
    }
  }

  void _handleInvalidPermissions(PermissionStatus permissionStatus) {
    if (permissionStatus == PermissionStatus.denied) {
      throw new PlatformException(
          code: "PERMISSION_DENIED",
          message: "Access to location data denied",
          details: null);
    } else if (permissionStatus == PermissionStatus.disabled) {
      throw new PlatformException(
          code: "PERMISSION_DISABLED",
          message: "Location data is not available on device",
          details: null);
    }
  }
}
